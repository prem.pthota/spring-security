package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Users;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Users saveUser(Users users) {
		
		System.out.println(users.getLoginId());
		// TODO Auto-generated method stub
		return userRepository.save(users);
	}

	@Override
	public List<Users> listUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public Users getByUserId(Integer id) {
		// TODO Auto-generated method stub
		return userRepository.getById(id);
	}

	@Override
	public void deleteByUserId(Integer id) {
		// TODO Auto-generated method stub
		Users users = userRepository.getById(id);
		if (users != null) {
			userRepository.delete(users);
		}

	}

}
