package com.example.demo.service;

import java.util.List;

import com.example.demo.domain.Users;

public interface UserService {

	public Users saveUser(Users users);

	public List<Users> listUsers();

	public Users getByUserId(Integer id);

	public void deleteByUserId(Integer id);

}
