package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Users;
import com.example.demo.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping({ "/demo" })
@Api(tags = { "Users API" })
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping("user/save")
	@ApiOperation(value = "Save or Update User")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, response = Users.class, message = "OK"),
			@ApiResponse(code = 400, message = "Bad Request"), //
			@ApiResponse(code = 403, message = "Access Denied") })
	public ResponseEntity<Users> saveUser(@RequestBody Users users) {
		System.out.println(users);
		
		return ResponseEntity.ok(userService.saveUser(users));
	}

	@GetMapping("user/list")
	@ApiOperation(value = "User List")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, response = Users.class, message = "OK"),
			@ApiResponse(code = 400, message = "Bad Request"), //
			@ApiResponse(code = 403, message = "Access Denied") })
	public ResponseEntity<List<Users>> listUsers() {
		return ResponseEntity.ok(userService.listUsers());
	}

	@GetMapping("user/{id}")
	@ApiOperation(value = "Get By User Id")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, response = Users.class, message = "OK"),
			@ApiResponse(code = 400, message = "Bad Request"), //
			@ApiResponse(code = 403, message = "Access Denied") })
	public ResponseEntity<Users> getByUserId(@PathVariable(name = "id") Integer id) {
		return ResponseEntity.ok(userService.getByUserId(id));
	}

	@DeleteMapping("user/{id}")
	@ApiOperation(value = "Delete By User Id")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, response = Users.class, message = "OK"),
			@ApiResponse(code = 400, message = "Bad Request"), //
			@ApiResponse(code = 403, message = "Access Denied") })
	public void deleteByUserId(@PathVariable(name = "id") Integer id) {
		System.out.println(id);
	}
}
