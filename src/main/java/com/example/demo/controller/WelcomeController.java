/**
 * 
 */
package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Polaiah Thota
 *
 */
@ApiIgnore
@Controller
public class WelcomeController {
	@RequestMapping(value = "/")
	public String page() {

		return "redirect:/swagger-ui.html";
	}

}
